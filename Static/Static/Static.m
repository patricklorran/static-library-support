//
//  Static.m
//  Static
//
//  Created by Patrick Lorran on 23/07/21.
//

#import "Static.h"

@implementation Static

+ (void)sayHello {
    NSLog(@"Hello, Static Library!");
}

@end
