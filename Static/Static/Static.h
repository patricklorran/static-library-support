//
//  Static.h
//  Static
//
//  Created by Patrick Lorran on 23/07/21.
//

#import <Foundation/Foundation.h>

@interface Static : NSObject

+ (void)sayHello;

@end
