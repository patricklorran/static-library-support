//
//  ContentView.swift
//  Sample
//
//  Created by Patrick Lorran on 23/07/21.
//

import Dynamic
import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Hello, world!")
                .padding()
        }
        .onAppear {
            Printer.sayHello()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
