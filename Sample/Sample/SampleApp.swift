//
//  SampleApp.swift
//  Sample
//
//  Created by Patrick Lorran on 23/07/21.
//

import SwiftUI

@main
struct SampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
