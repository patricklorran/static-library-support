//
//  Printer.h
//  Dynamic
//
//  Created by Patrick Lorran on 26/07/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Printer : NSObject

+ (void)sayHello;

@end

NS_ASSUME_NONNULL_END
