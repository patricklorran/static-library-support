//
//  Dynamic.h
//  Dynamic
//
//  Created by Patrick Lorran on 23/07/21.
//

#import <Foundation/Foundation.h>

//! Project version number for Dynamic.
FOUNDATION_EXPORT double DynamicVersionNumber;

//! Project version string for Dynamic.
FOUNDATION_EXPORT const unsigned char DynamicVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Dynamic/PublicHeader.h>

#import <Dynamic/Printer.h>
