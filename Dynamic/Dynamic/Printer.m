//
//  Printer.m
//  Dynamic
//
//  Created by Patrick Lorran on 26/07/21.
//

#import "Printer.h"

#import "Static.h"

@implementation Printer

+ (void)sayHello {
    [Static sayHello];
}

@end
